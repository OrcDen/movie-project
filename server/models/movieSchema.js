const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const movieSchema = new Schema({
    name: { type: String },
    cost: { type: Number },
    price: { type: Number },
    genre: { type: String },
    rating: { type: Number },
    stock: { type: Number }
});

module.exports = mongoose.model('movie', movieSchema );
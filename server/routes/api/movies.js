const express = require('express');
const mongodb = require('mongodb');
const mongoose = require('mongoose');
const Movie = require('../../models/movieSchema.js');

mongoose.connect("mongodb://tlc_pet_food:password1@ds023052.mlab.com:23052/tlc_pet_food_project", { useNewUrlParser: true });
const router = express.Router();

//Get All Movies
router.route( '/' ).get( ( req, res ) => {
    Movie.find({}, (err, movies) => {
        res.json( movies );
    })
});

//Get Movie by ID
router.route( '/:id' ).get( (req, res) => {
    Movie.findById( new mongodb.ObjectId( req.params.id ), ( err, movie ) => {
        res.json( movie );
    });
});

//Add Movie
router.route( '/' ).post( ( req, res ) => {
    var movie = new Movie({
        name: req.body.name,
        cost: req.body.cost,
        price: req.body.price,
        genre: req.body.genre,
        rating: req.body.rating,
        stock: req.body.stock
    });
    movie.save();
    res.status(201).send(movie);
});

//Delete Movie
router.route( '/:id' ).delete( ( req,res )=> {
    Movie.findById( new mongodb.ObjectId( req.params.id ), ( err, movie ) => {
        movie.remove( err => {
            if(err){
                res.status(500).send(err);
            }
            else {
                res.status(204).send('removed');
            }
        });
    });
});

//Update Movie by id
router.route( '/:id' ).put( (req, res) => {
    Movie.findById(new mongodb.ObjectId( req.params.id ), ( err, movie ) => {
        if( req.body._id ){
            delete req.body._id;
        }
        for( var i in req.body ) {
            movie[i] = req.body[i];
        }
        movie.save();
        res.json(movie);
    })
});

module.exports = router;
# server

## Project setup
First in the Root Folder
```
npm install

```
Then
```
cd client
npm install
```

## Development Commands with hot-reloads for development

###From Root Folder
```
npm run dev
```
Starts Backend on port 5000

###From Client Folder
```
npm run serve
```
Starts Front End on port 8080



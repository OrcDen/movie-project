import MovieComponent from './components/MovieComponent.vue'
import SalesComponent from './components/SalesComponent.vue'
import EditMovie from './components/EditMovie.vue'
import AddMovie from './components/AddMovie.vue'

export default [
    { path: '/', component: MovieComponent },
    { path: '/sales', component: SalesComponent },
    { path: '/edit-movie/:id', component: EditMovie },
    { path: '/add-movie', component: AddMovie }
]
import axios from 'axios';

const url = '/api/movies/';

class MovieService {
    // Get Movies
    static getMovies() {
        return new Promise( async ( resolve, reject ) => {
            try {
                const res = await axios.get( url );
                const data = res.data;
                resolve( 
                    data.map(movie => ({
                        ...movie
                    }))
                 );
            } catch ( err ) {
                reject( err );
            }
        } )
    }

    // Get Movie by Id
    static getMovieById( id ) {
        return new Promise( async ( resolve, reject ) => {
            try {
                const res = await axios.get( `${ url }${ id }` );
                resolve( res.data );
            } catch ( err ) {
                reject( err );
            }
        } )
    }

    // Create new Movie
    static insertMovie( name, cost, price, genre, rating, stock ) {
        return new Promise( async ( resolve, reject ) => {
            try {
                const res = await axios.post( url, {
                    name,
                    cost,
                    price,
                    genre,
                    rating,
                    stock
                } );
                resolve( res.data );
            } catch ( err ) {
                reject( err );
            }
        } )
    }

    // Update Movie
    static updateMovie( id, data ){
        return new Promise( async ( resolve, reject ) => {
            try {
                const res = axios.put( `${ url }${ id }`, data );
                resolve( res.data );
            } catch ( err ) {
                reject( err );
            }
        } )
        
    }

    // Delete Movie
    static deleteMovie( id ) {
        return axios.delete( `${ url }${ id }` );
    }
}

export default MovieService;